import "@aws-amplify/ui-react/styles.css";
import { API, Storage } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
import '../App.css';
import { createNote as createNoteMutation, deleteNote as deleteNoteMutation } from '../graphql/mutations';
import { listNotes } from '../graphql/queries';
import { TextField, Button, Card, Grid } from '@aws-amplify/ui-react';
import { Menu, MenuItem, View } from '@aws-amplify/ui-react';

const initialFormState = { name: '', description: '' }

export const BasicExample = () => {
  return (
    <View width="4rem">
      <Menu>
        <MenuItem>Option 1</MenuItem>
        <MenuItem>Option 2</MenuItem>
        <MenuItem>Option 3</MenuItem>
      </Menu>
    </View>
  );
};


export default function Notes() {
  const [notes, setNotes] = useState([]);
  const [formData, setFormData] = useState(initialFormState);

  useEffect(() => {
    fetchNotes();
  }, []);

  async function fetchNotes() {
    const apiData = await API.graphql({ query: listNotes });
    const notesFromAPI = apiData.data.listNotes.items;
    await Promise.all(notesFromAPI.map(async note => {
      if (note.image) {
        const image = await Storage.get(note.image);
        note.image = image;
      }
      return note;
    }))
    setNotes(apiData.data.listNotes.items);
  }
  async function createNote() {
    if (!formData.name || !formData.description) return;
    await API.graphql({ query: createNoteMutation, variables: { input: formData } });
    if (formData.image) {
      const image = await Storage.get(formData.image);
      formData.image = image;
    }
    setNotes([ ...notes, formData ]);
    setFormData(initialFormState);
  }

  async function deleteNote({ id }) {
    const newNotesArray = notes.filter(note => note.id !== id);
    setNotes(newNotesArray);
    await API.graphql({ query: deleteNoteMutation, variables: { input: { id } }});
  }

  async function onChange(e) {
    if (!e.target.files[0]) return
    const file = e.target.files[0];
    setFormData({ ...formData, image: file.name });
    await Storage.put(file.name, file);
    fetchNotes();
  }

  return (
    <Grid
        columnGap="0.5rem"
        rowGap="0.5rem"
        templateColumns="1fr 1fr"
        templateRows="1fr 1fr">
      <Card>
        <TextField
          size="small"
          onChange={e => setFormData({ ...formData, 'name': e.target.value})}
          descriptiveText="Enter a valid Note name"
          placeholder="Note name"
          label="Note name"
          labelHidden
          errorMessage="There is a note name error"
        />
        <TextField
          size="small"
          onChange={e => setFormData({ ...formData, 'description': e.target.value})}
          descriptiveText="Enter a valid Note description"
          placeholder="Note description"
          label="Note description"
          labelHidden
          errorMessage="There is a note description error"
        />
        <input
          type="file"
          onChange={onChange}
        />
        <button onClick={createNote}>Create Note</button>
      </Card>
      <Card>
        <div style={{marginBottom: 30}}>
          {
            notes.map(note => (
              <div key={note.id || note.name}>
                <h2>{note.name}</h2>
                <p>{note.description}</p>
                <button onClick={() => deleteNote(note)}>Delete note</button>
                {
                  note.image && <img src={note.image} style={{width: 400}} />
                }
              </div>
            ))
          }
        </div>
      </Card>
    </Grid>
  );
}