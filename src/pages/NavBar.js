import { FiHome, FiInfo, FiUser, FiChevronsRight, FiChevronsLeft } from 'react-icons/fi';
import CustomLink from './CustomLink'

export default function NavBar({ sideBarActive, onToggleSideBarClick }) {
	return (
		<nav className="nav">
			{sideBarActive ? (
				<FiChevronsLeft className="icon-rotate" onClick={onToggleSideBarClick}/>
			) : (
				<FiChevronsRight className="icon-rotate" onClick={onToggleSideBarClick}/>
			)}
			<ul>
				<CustomLink to="/home"><FiHome className="icon-rotate-fast" /></CustomLink>
				<CustomLink to="/about"><FiInfo className="icon-rotate-fast" /></CustomLink>
				<CustomLink to="/user"><FiUser className="icon-rotate-fast" /></CustomLink>
			</ul>
		</nav>
	)
}
