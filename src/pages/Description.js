import { TextAreaField } from '@aws-amplify/ui-react';

export default function Input({handleChange}) {
    return <TextAreaField
        autoComplete="off"
        descriptiveText="Enter a valid description"
        direction="column"
        hasError={false}
        isDisabled={false}
        isReadOnly={false}
        isRequired={false}
        label="Description"
        labelHidden={false} 
        name="description"
        placeholder="Description"
        rows="3"
        size="small"
        wrap="nowrap"
        onChange={handleChange}
        onInput={(e) => console.info('input fired:', e.currentTarget.value)}
        onCopy={(e) => console.info('onCopy fired:', e.currentTarget.value)}
        onCut={(e) => console.info('onCut fired:', e.currentTarget.value)}
        onPaste={(e) => console.info('onPaste fired:', e.currentTarget.value)}
        onSelect={(e) =>
            console.info(
                'onSelect fired:',
                e.currentTarget.value.substring(
                    e.currentTarget.selectionStart,
                    e.currentTarget.selectionEnd
                )
            )
        }
    />
}