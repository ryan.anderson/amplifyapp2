
import { API, Storage } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
//import '../App.css';
import { createNote as createNoteMutation, deleteNote as deleteNoteMutation } from '../graphql/mutations';
import { listNotes } from '../graphql/queries';
import { TextField, Button, Card, Grid } from '@aws-amplify/ui-react';
import Description from './Description.js'
//import { Menu, MenuItem, View } from '@aws-amplify/ui-react';

const initialFormState = { name: '', description: '' }

export default function Input() {
  const [ enabled, setEnabled ] = useState(false);
  const [ notes, setNotes ] = useState([]);
  const [ formData, setFormData ] = useState(initialFormState);

  useEffect(() => {
    fetchNotes();
  }, []);

  async function fetchNotes() {
    const apiData = await API.graphql({ query: listNotes });
    const notesFromAPI = apiData.data.listNotes.items;
    await Promise.all(notesFromAPI.map(async note => {
      if (note.image) {
        const image = await Storage.get(note.image);
        note.image = image;
      }
      return note;
    }))
    setNotes(apiData.data.listNotes.items);
  }
  async function createNote() {
    if (!formData.name || !formData.description) return;
    await API.graphql({ query: createNoteMutation, variables: { input: formData } });
    if (formData.image) {
      const image = await Storage.get(formData.image);
      formData.image = image;
    }
    setNotes([ ...notes, formData ]);
    setFormData(initialFormState);
    setEnabled(false);
  }

  async function deleteNote({ id }) {
    const newNotesArray = notes.filter(note => note.id !== id);
    setNotes(newNotesArray);
    await API.graphql({ query: deleteNoteMutation, variables: { input: { id } }});
  }

  async function onChange(e) {
    if (!e.target.files[0]) return
    const file = e.target.files[0];
    setFormData({ ...formData, image: file.name });
    await Storage.put(file.name, file);
    fetchNotes();
  }

  return (
    <div className="input">
      <button onClick={() => setEnabled(true)} disabled={ enabled }>Add</button>
      <button onClick={() => setEnabled(false)} disabled={ !enabled }>Cancel</button>
      {enabled && (
        <Card className="input">
          <TextField
            size="small"
            onChange={e => setFormData({ ...formData, 'name': e.target.value})}
            descriptiveText="Enter a valid Trip name"
            placeholder="Trip name"
            label="Trip name"
            errorMessage="There is an error with Trip name"
          />
          <Description handleChange={e => setFormData({ ...formData, 'description': e.currentTarget.value})}/>
          <input
            type="file"
            onChange={onChange}
          />
          <button onClick={() => setEnabled(false)}>Cancel</button>
          <button onClick={createNote}>Create</button>
        </Card>
      )}
    </div>
  );
}