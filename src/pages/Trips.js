import { Flex, View, Button, useTheme } from '@aws-amplify/ui-react'
import {Link, useParams, useNavigate} from 'react-router-dom'
import tripData from '../database/trips.json'



const findTrip = (key) => {
    if(!key) return null;
    return tripData.users[0].trips.find((trip) => trip.key === key)
}

// const handleOnClick = (key) => {
//     navigate.push(`/trip/${key}`)
// }

export default function Trips() {
    const navigate = useNavigate()
    const { tokens } = useTheme()
    const { key: tripId } = useParams()
    const tripFound = findTrip(tripId)
  
    return (
        <>
            <h1>Trips</h1>
            {tripFound ? <h3>{tripFound.name}</h3> : null}
            <Flex
                direction="row"
                justifyContent="flex-start"
                alignItems="flex-start"
                alignContent="flex-start"
                wrap="wrap"
                gap="1rem"
                >
            {tripData.users[0].trips.map(({name, key}, index) => {
                // <Link to={`/trip/${key}`}>
                return (
                    <View 
                        height="10rem"
                        width="10rem"
                        backgroundColor={tokens.colors.neutral[20]}
                        key={index}
                    >
                        <Flex>
                            <Button
                                size="small"
                                loadingText="hold up"
                                onClick={() => navigate(`/trip/${key}`)}
                                ariaLabel=""
                            >{name}</Button>
                        </Flex>
                    </View>
                )
            })}
            </Flex>
        </>
    )
}
