import { useState } from 'react'
import { Link, Route, Routes } from 'react-router-dom'
import { withAuthenticator } from '@aws-amplify/ui-react'
import classNames from 'classnames'
import NavBar from './pages/NavBar'
import Home from './pages/Home'
import Notes from './pages/Notes'
import About from './pages/About'
import Pricing from './pages/Pricing'
import User from './pages/User'
import Input from './pages/Input'
import BasicDemo from './pages/BasicDemo'
import AdvancedDemo from './pages/AdvancedDemo'
import Trips from './pages/Trips'
import NotFound from './pages/NotFound'
import CustomLink from './pages/CustomLink'

import '@aws-amplify/ui-react/styles.css'
import '@fontsource/inter/variable.css'


import { TextField, Button, Card, Grid } from '@aws-amplify/ui-react';

// const initialFormState = { name: '', description: '' }


function App({ signOut }) {

  const [sideBarActive, setSideBarActive] = useState(false);

  const wrapperSideBarClass = classNames({ 'sidebar-open': sideBarActive, 'sidebar-closed': !sideBarActive });
  const wrapperMainSideBarClass = classNames({ 'main-sidebar-open': sideBarActive, 'main-sidebar-closed': !sideBarActive });

  const onToggleSideBarClick = function() {
    setSideBarActive(current => !current)
  }

  return (
    <div id="main" className={wrapperMainSideBarClass}>
      <NavBar onToggleSideBarClick={onToggleSideBarClick} sideBarActive={sideBarActive}/>

      <div id="mySidebar" className={wrapperSideBarClass}>
        <CustomLink to='/trips'>Trips</CustomLink>
      </div>

      <div className="container">
        <Routes>
          <Route path="*" element={<NotFound />} />
          <Route path="/home" element={<Home />} />
          <Route path="/notes" element={<Notes />} />
          <Route path="/about" element={<About />} />
          <Route path="/pricing" element={<Pricing />} />
          <Route path="/user" element={<User />} />
          <Route path="/input" element={<Input />} />
          <Route path="/bd" element={<BasicDemo />} />
          <Route path="/ad" element={<AdvancedDemo />} />
          <Route path="/trips" element={<Trips />} />
          <Route path="/trip/:key" element={<Trips />} />
        </Routes>
      </div>
    </div>
  )
}
export default withAuthenticator(App);
